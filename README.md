# Like lion 최종 프로젝트_ 개인
# 멋사스네스(멋사sns)

# SWAGGER 주소 : http://13.125.159.165:8080/swagger-ui/
# Endpoint

* POST api/v1/users/join _ 회원가입
* POST api/v1/users/login _ 로그인
* GET api/v1/posts _ 포스트 리스트 확인
* GET api/v1/posts/{id} _ 포스트 상세 확인
* POST api/v1/posts _ 포스트 작성
* PUT api/v1/posts/{id} 포스트 수정
* DELETE api/v1/posts/{id} 포스트 삭제

### 1주차 미션 요구사항 분석 & 체크리스트
* * *
### 221220
1. CI/CD 구축 ✅
    - repository에 새로운 내용이 push 됐다면 해당 내용을 포함하여 자동으로 도커 이미지를 업데이트함
2. GET api/v1/hello endpoint 작성 ✅
    - 접속시 hello 출력
3. 도커를 이용한 배포 및 배포 자동화 ✅
    - crontab 을 이용하여 도커 이미지가 업데이트 되었을때만 해당 도커 컨테이너를 내리고 재시작
* * *
### 221221
1. DB 설계
    - 주어진 대로 작성한다. ->User만 완료 ✅
2. 회원가입 구현(POST api/v1/users/join)
    - request body로 아이디와 비밀번호를 받는다. ✅
    - 아이디를 중복확인한다. ✅
        - 중복이라면 DUPLICATED_USER_NAME 에러코드를 반환한다. ✅
        - 중복이 아니라면 비밀번호를 암호화 한 뒤 해당 내용을 회원 DB에 저장한다. ✅
3. 로그인 구현(POST api/v1/users/login)
    - request body로 아이디와 비밀번호를 받는다. ✅
    - 로그인 유효성을 판단한다. ✅
        - 아이디가 있는지 확인한다. ✅
            - 있다면 다음으로 넘어가고, 없다면 USERNAME_NOT_FOUND 에러코드를 반환한다. ✅
        - 아이디와 비밀번호가 일치하는지 확인한다. ✅
            - 비밀번호가 일치하지 않는다면 INVALID_PASSWORD 에러코드를 반환한다. ✅
4. 테스트 코드 작성 ✅
    - 2번과 3번 내용에 대해 성공/실패 테스트 코드를 작성한다. ✅

* * *
### 221222
1. DB 설계
   - 주어진 대로 작성한다. -> post 완료 ✅
2. 포스트 상세 내역 보기 구현(GET /posts/{id}) ✅
   * 회원이 아니어도 볼 수 있다. ✅
   * id, 제목, 작성자, 작성날짜, 수정날짜, 내용, (추후 댓글리스트, 좋아요) 를 리턴한다. ✅
3. 포스트 등록 구현(POST /posts) ✅
   * 회원만 할 수 있다 -> 토큰 검증 필요 ✅
   * 제목 내용 글쓴이(토큰 분석을 통해)를 저장한다. ✅
4. 테스트 코드 작성 ✅
   * 2_ 조회 성공 (id, title, body, userName의 exist 여부 반환) ✅
   * 1_ 포스트 작성 성공 ✅
   * 1_ 포스트 작성 실패 ✅
     * 토큰 없이 작성한 경우 ✅
     * 토큰이 유효하지 않은 경우 ✅
     * 유저가 존재하지 않는 경우 ✅

* * *
### 221223
1. 포스트 수정 구현(PUT /posts/{id}) ✅
   * 회원이어야 수정 가능하다. 토큰 검증 필요 ✅
   * 자신이 작성한 글 또는 관리자인 상황에서만 수정 가능하다 ✅
2. 포스트 삭제 구현(DELETE /posts) ✅
   * 회원만 할 수 있다 -> 토큰 검증 필요 ✅
   * 자신이 작성한 글 또는 관리자인 상황에서만 삭제 가능하다 ✅
3. 테스트 코드 작성 ✅
   
   수정

   controller
   * 포스트 수정 실패(1) : 인증 실패 ✅
   * 포스트 수정 실패(2) : 작성자 불일치 ✅
   * 포스트 수정 실패(3) : 데이터베이스 에러 
   * 포스트 수정 성공 ✅
      * 토큰 없이 작성한 경우 ✅
      * 토큰이 유효하지 않은 경우 ✅
      * 유저가 존재하지 않는 경우 ✅

   service
   * 수정 실패 : 포스트 존재하지 않음 ✅
   * 수정 실패 : 작성자!=유저 ✅
   * 수정 실패 : 유저 존재하지 않음 ✅

   
   삭제

   controller

   * 포스트 삭제 성공 
   * 포스트 삭제 실패(1) : 인증 실패 ✅
   * 포스트 삭제 실패(2) : 작성자 불일치 ✅
   * 포스트 삭제 실패(3) : 데이터베이스 에러

   service
   
   * 삭제 실패 : 유저 존재하지 않음 ✅
   * 삭제 실패 : 포스트 존재하지 않음 ✅

* * *
### 221226
1. 포스트 리스트 보기 구현(Get /posts) ✅
   * 최신순으로 정렬돼야 한다. ✅
   * 한 페이지당 20개의 글이 보여야한다. ✅
2. 테스트 코드 작성

   controller

   *조회 성공 : 0번이 1번보다 날짜가 최신 ✅

* * *
### 1주차 미션 수행 과정
* * *
### 221220
##### [접근 방법]
1. [🎈강사님 유튜브](https://www.youtube.com/watch?v=sAGwCB541H4)
2. [☕CI/CD 구축하기](https://jmholly.tistory.com/entry/CICD-GitLab-CICD-%EA%B5%AC%EC%B6%95%ED%95%98%EA%B8%B07-GitLab-docker-%EB%A1%9C%EA%B7%B8%EC%9D%B8-%EB%AC%B8%EC%A0%9C-%ED%95%B4%EA%B2%B0%ED%95%98%EA%B8%B0-docker-push-%ED%95%98%EB%A9%B4-denied-access-forbidden)
##### [특이사항]
1. CI/CD를 구죽하면서 많은 오류를 겪었는데 대부분 환경변수 문제였다. 특히 프로젝트명을 전부 소문자로 작성해야한다는 점이 발견하기 어려웠다.
2. 또한 private repository라면 도커 이미지를 서버에 pull 하는 과정에서 permission denied가 발생하는데 이 부분을 해결하는데에도 꽤 많은 시간이 걸렸다.
* * *
### 221221
##### [접근 방법]
* user eneity 작성 시 등록 시간, 업데이트 시간 등을 저장한 컬럼의 type 지정시 참고한 링크
1. [🍕[Java] 날짜 타입 클래스(LocalDate, LocalTime, LocalDateTime)](https://footprint-of-nawin.tistory.com/67)
2. [🍔2How to map MySQL Timestamp field in Spring JAVA(JPA/Hibernate)](https://stackoverflow.com/questions/63394673/how-to-map-mysql-timestamp-field-in-spring-javajpa-hibernate)
3. [🍳[JPA] 필드와 컬럼 매핑 - @Temporal](https://ttl-blog.tistory.com/116)

시간 관련된 컬럼의 자료형은 LocalDateTime으로 지정하게 되었다.
* mockmvc test 를 작성하기 위해 참고한 링크
1. [🍜[Test] Mockito를 이용한 단위 테스트](https://velog.io/@wlsh44/symr7p68)
2. [🧂MockMvc를 이용한 REST API의 Json Response 검증](https://ykh6242.tistory.com/entry/MockMvc%EB%A5%BC-%EC%9D%B4%EC%9A%A9%ED%95%9C-REST-API%EC%9D%98-Json-Response-%EA%B2%80%EC%A6%9D)

##### [특이사항]
1. mock test를 이용하다 보니 빈 객체를 리턴하게 하도록 한 뒤 테스트를 진행하도록 하게 설계해놓고 test시에는 해당 객체 내의 변수들을 조회하여 테스트가 통과되지 않는 버그가 있었다. 이를 이미 선언된 객체를 리턴하게 하는 것으로 해결하였다.
* * *
### 221222
##### [접근 방법]
* 포스트 상세 내역 보기 구현 

시간 형식을 밀리초를 빼고 리턴해야했다. dto에서의 JsonFormat 어노테이션을 이용해 이를 해결했다.

1. [😀[spring] LocalDateTime 주고받기(Response, Request)](https://m.blog.naver.com/PostView.naver?isHttpsRedirect=true&blogId=varkiry05&logNo=221736856257)

* 포스트 등록 구현

토큰에 관련한 에러처리를 하려고 할때 filter에서 커스텀 에러를 throw하려고 하니 작동이 되지 않았다.
필터에서는 request에 exception이라는 변수를 붙여주는 것으로 처리하고, 해당 exception 종류에 따라 customEntryPoint에서 response를 작성하여 리턴하는것으로 해결했다.

1. [😁Spring security JWT filter throws 500 and HTML instead of 401 and json](https://stackoverflow.com/questions/62903367/spring-security-jwt-filter-throws-500-and-html-instead-of-401-and-json)
2. [🤣[08] mutsa-SNS 3일차 - (3) JwtTokenFilter Exception 추가](https://celdan.tistory.com/11)
3. [😋Spring Security JWT 토큰 검증 시 Exception 예외 처리](https://beemiel.tistory.com/11)

##### [특이사항]
* 서버에서 발생한 오류

포스트 등록을 구현하고 나니 로컬에서는 잘 돌아가고 서버에서는 돌아가지 않는 오류가 발생했다.
해당 내용은 JSONException을 사용하다보니 발생했는데 잘못된 import 문을 사용한것이 원인이었다.

1. [😥Caused by: java.lang.NoClassDefFoundError: org/springframework/boot/configurationprocessor/json/JSONException](https://velog.io/@stella6767/Caused-by-java.lang.NoClassDefFoundError-orgspringframeworkbootconfigurationprocessorjsonJSONException)
### 221223
##### [접근 방법]
* 포스트 수정 구현

수정도 등록과 마찬가지로 repository.save()를 활용하면 간단히 작성이 가능하다. 등록과 다른 점은 글 작성자가 로그인된 회원과 일치하는지를 판단하여 수정권한을 주어야하는데, 이는 토큰에서 얻은 userId를 통해 user정보를 검색하고, 해당글의 user정보와 비교하는것으로 간단히 해결이 가능하다.

[😶Save는 Insert와 Update를 어떻게 구분할까](https://brunch.co.kr/@anonymdevoo/37)

##### [특이사항]
* 수정을 할때 실행되는 sql쿼리문을 보면 update가 되기 전에 select가 먼저 실행되는것을 볼 수 있다.

[🙄JPA에서 save할때 select 쿼리가 먼저 실행되는 이유](https://programmer-chocho.tistory.com/80)

### 221226
##### [접근 방법]
* 포스트 리스트 보기 구현

리턴 형식을 맞추기 어려웠는데 팀원의 도움을 받아 해결했다.

##### [특이사항]
* 오류가 리턴되는 경우가 발생했는데, 엄청 긴 오류를 쭉 내려보니 type definition error였다.

[😛[JPA] FetchType.Lazy로 인한 JSON 오류 (InvalidDefinitionException: No serializer found for class)](https://ahndding.tistory.com/24)

해당 블로그 글의 2번 방법을 사용하여 Post의 User컬럼에 @JsonIgnore를 붙이는 것으로 해결했다.

