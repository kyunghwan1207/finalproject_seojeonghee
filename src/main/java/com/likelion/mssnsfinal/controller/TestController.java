package com.likelion.mssnsfinal.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class TestController {

    @GetMapping("/hello")
    public ResponseEntity<String> printHello(){
        return ResponseEntity.ok().body("서정희");
    }

    @GetMapping("/hello2")
    public ResponseEntity<String> printHello2(){
        return ResponseEntity.ok().body("hello2");
    }
}
