package com.likelion.mssnsfinal.service;

import com.likelion.mssnsfinal.domain.UserRole;
import com.likelion.mssnsfinal.domain.dto.PostEnrollAndModifyRequest;
import com.likelion.mssnsfinal.domain.dto.PostInfoResponse;
import com.likelion.mssnsfinal.domain.entity.Post;
import com.likelion.mssnsfinal.domain.entity.User;
import com.likelion.mssnsfinal.exception.FinalProjectAppException;
import com.likelion.mssnsfinal.repository.PostRepository;
import com.likelion.mssnsfinal.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.Authentication;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class PostServiceTest {
    PostService postService;

    PostRepository postRepository = mock(PostRepository.class);
    UserRepository userRepository = mock(UserRepository.class);

    @BeforeEach
    void setUp() {
        postService = new PostService(postRepository,userRepository);

    }

    @Test
    @DisplayName("get post success")
    void get_post_success() {

        User user = User.builder()
                .userId(1)
                .userName("헝지")
                .password("1234")
                .role(UserRole.USER)
                .registeredAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .build();

        Post post = Post.builder()
                .id(1)
                .user(user)
                .title("title")
                .body("body")
                .createdAt(LocalDateTime.now())
                .lastModifiedAt(LocalDateTime.now())
                .build();


        given(postRepository.findById(post.getId())).willReturn(Optional.of(post));
        PostInfoResponse postInfoResponse = postService.getPost(post.getId());
        assertEquals(user.getUserName(), postInfoResponse.getUserName());
    }



    @Test
    void getAllPosts() {
    }

    @Test
    @DisplayName("enroll post success")
    void enroll_post_success() {

        User user = User.builder()
                .userId(1)
                .userName("헝지")
                .password("1234")
                .role(UserRole.USER)
                .registeredAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .build();

        Post post = Post.builder()
                .id(1)
                .user(user)
                .title("title")
                .body("body")
                .createdAt(LocalDateTime.now())
                .lastModifiedAt(LocalDateTime.now())
                .build();

        when(userRepository.findById(user.getUserId()))
                .thenReturn(Optional.of(user));
        when(postRepository.save(any()))
                .thenReturn(post);

        Assertions.assertDoesNotThrow(() -> postService.enrollPost(new PostEnrollAndModifyRequest(post.getTitle(), post.getBody()),user.getUserId().toString()));
    }

    @Test
    @DisplayName("enroll post fail")
    void enroll_post_fail() {
        User mockUser = mock(User.class);
        PostEnrollAndModifyRequest mockPostEnrollRequest = mock(PostEnrollAndModifyRequest.class);
        when(userRepository.findById(any()))
                .thenReturn(Optional.empty());

        FinalProjectAppException exception = assertThrows(FinalProjectAppException.class,
                ()-> {
                    postService.enrollPost(mockPostEnrollRequest, mockUser.getUserId().toString());
                });

        Assertions.assertEquals("USERNAME_NOT_FOUND", exception.getErrorCode().name());

    }

    @Test
    @DisplayName("modify post fail_post not exist")
    void modify_post_fail_post_not_exist() {
        User mockUser = mock(User.class);
        Post mockPost = mock(Post.class);
        PostEnrollAndModifyRequest mockPostModifyRequest = mock(PostEnrollAndModifyRequest.class);
        when(postRepository.findById(any()))
                .thenReturn(Optional.empty());
        when(userRepository.findById(any()))
                .thenReturn(Optional.ofNullable(mockUser));

        FinalProjectAppException exception = assertThrows(FinalProjectAppException.class,
                ()-> {
                    postService.modifyPost(mockPost.getId(),mockPostModifyRequest,mockUser.getUserId().toString());
                });

        Assertions.assertEquals("POST_NOT_FOUND", exception.getErrorCode().name());
    }
    @Test
    @DisplayName("modify post fail_user not match")
    void modify_post_fail_user_not_match() {
        User mockUser = mock(User.class);
        Post postByMockUser = Post.builder()
                .id(1)
                .title("title")
                .body("body")
                .createdAt(LocalDateTime.now())
                .lastModifiedAt(LocalDateTime.now())
                .user(mockUser)
                .build();
        User user = User.builder()
                .userId(1)
                .userName("헝지")
                .password("1234")
                .role(UserRole.USER)
                .registeredAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .build();
        PostEnrollAndModifyRequest mockPostModifyRequest = mock(PostEnrollAndModifyRequest.class);
        when(postRepository.findById(any()))
                .thenReturn(Optional.of(postByMockUser));
        when(userRepository.findById(any()))
                .thenReturn(Optional.of(user));
        FinalProjectAppException exception = assertThrows(FinalProjectAppException.class,
                ()-> {
                    postService.modifyPost(postByMockUser.getId(),mockPostModifyRequest, user.getUserId().toString());
                });

        Assertions.assertEquals("INVALID_PERMISSION", exception.getErrorCode().name());
    }

    @Test
    @DisplayName("modify post fail_user not exist")
    void modify_post_fail_user_not_exist() {
        User mockUser = mock(User.class);
        Post postByMockUser = Post.builder()
                .id(1)
                .title("title")
                .body("body")
                .createdAt(LocalDateTime.now())
                .lastModifiedAt(LocalDateTime.now())
                .user(mockUser)
                .build();
        PostEnrollAndModifyRequest mockPostModifyRequest = mock(PostEnrollAndModifyRequest.class);
        when(postRepository.findById(any()))
                .thenReturn(Optional.of(postByMockUser));
        when(userRepository.findById(any()))
                .thenReturn(Optional.empty());
        FinalProjectAppException exception = assertThrows(FinalProjectAppException.class,
                ()-> {
                    postService.modifyPost(postByMockUser.getId(),mockPostModifyRequest, mockUser.getUserId().toString());
                });

        Assertions.assertEquals("USERNAME_NOT_FOUND", exception.getErrorCode().name());
    }

    @Test
    @DisplayName("delete post fail_post not exist")
    void delete_post_fail_post_not_exist() {
        User mockUser = mock(User.class);
        Post mockPost = mock(Post.class);
        when(postRepository.findById(any()))
                .thenReturn(Optional.empty());
        when(userRepository.findById(any()))
                .thenReturn(Optional.ofNullable(mockUser));

        FinalProjectAppException exception = assertThrows(FinalProjectAppException.class,
                ()-> {
                    postService.deletePost(mockPost.getId(),mockUser.getUserId().toString());
                });

        Assertions.assertEquals("POST_NOT_FOUND", exception.getErrorCode().name());
    }

    @Test
    @DisplayName("delete post fail_user not exist")
    void delete_post_fail_user_not_exist() {
        User mockUser = mock(User.class);
        Post postByMockUser = Post.builder()
                .id(1)
                .title("title")
                .body("body")
                .createdAt(LocalDateTime.now())
                .lastModifiedAt(LocalDateTime.now())
                .user(mockUser)
                .build();
        when(postRepository.findById(any()))
                .thenReturn(Optional.of(postByMockUser));
        when(userRepository.findById(any()))
                .thenReturn(Optional.empty());
        FinalProjectAppException exception = assertThrows(FinalProjectAppException.class,
                ()-> {
                    postService.deletePost(postByMockUser.getId(), mockUser.getUserId().toString());
                });

        Assertions.assertEquals("USERNAME_NOT_FOUND", exception.getErrorCode().name());
    }
}