package com.likelion.mssnsfinal.repository;

import com.likelion.mssnsfinal.domain.entity.Post;
import com.likelion.mssnsfinal.domain.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.awt.print.Pageable;
import java.util.List;

public interface PostRepository extends JpaRepository<Post, Integer> {
    List<Post> findByUser(User user);
}
