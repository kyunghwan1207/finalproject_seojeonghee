package com.likelion.mssnsfinal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsSnsFinalApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsSnsFinalApplication.class, args);
	}

}
