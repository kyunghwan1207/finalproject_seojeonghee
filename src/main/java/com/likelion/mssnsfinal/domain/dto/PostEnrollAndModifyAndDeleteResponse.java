package com.likelion.mssnsfinal.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class PostEnrollAndModifyAndDeleteResponse {
    private String message;
    private Integer postId;

}
