package com.likelion.mssnsfinal.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.likelion.mssnsfinal.domain.dto.*;
import com.likelion.mssnsfinal.exception.ErrorCode;
import com.likelion.mssnsfinal.exception.FinalProjectAppException;
import com.likelion.mssnsfinal.service.UserService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
class UserControllerTest {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    UserService userService;
    @Autowired
    ObjectMapper objectMapper;

    @Test
    @DisplayName("Join success")
    @WithMockUser
    void join_success() throws Exception{
        UserJoinRequest userJoinRequest = UserJoinRequest.builder()
                .userName("헝지")
                .password("123")
                .build();
        UserDto userDto = UserDto.builder()
                .userName("헝지")
                .userId(0)
                .password("123")
                .build();
        when(userService.join(any())).thenReturn(userDto);

        mockMvc.perform(post("/api/v1/users/join")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(userJoinRequest)))
                        .andDo(print())
                        .andExpect(status().isOk())
                .andExpect(jsonPath("$.resultCode").value("SUCCESS"))
                .andExpect(jsonPath("$.result.userId").exists())
                .andExpect(jsonPath("$.result.userName").exists());
    }

    @Test
    @DisplayName("join failed : DUPLICATED_USERNAME")
    @WithMockUser
    void join_fail() throws Exception {
        UserJoinRequest userJoinRequest = UserJoinRequest.builder()
                .userName("헝지")
                .password("123")
                .build();
        when(userService.join(any())).thenThrow(new FinalProjectAppException(ErrorCode.DUPLICATED_USER_NAME, ErrorCode.DUPLICATED_USER_NAME.getMessage()));

        mockMvc.perform(post("/api/v1/users/join")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(userJoinRequest)))
                .andDo(print())
                .andExpect(status().isConflict())
                .andExpect(jsonPath("$.resultCode").value("ERROR"))
                .andExpect(jsonPath("$.result.errorCode").value(ErrorCode.DUPLICATED_USER_NAME.toString()))
                .andExpect(jsonPath("$.result.message").value(ErrorCode.DUPLICATED_USER_NAME.getMessage()));
    }

    @Test
    @DisplayName("login success")
    @WithMockUser
    void login_success() throws Exception {
        UserLoginRequest userLoginRequest = UserLoginRequest.builder()
                .userName("헝지")
                .password("1234")
                .build();
        when(userService.login(any(),any())).thenReturn("token");
        mockMvc.perform(post("/api/v1/users/login")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(userLoginRequest)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.resultCode").value("SUCCESS"))
                .andExpect(jsonPath("$.result.jwt").exists());

    }

    @Test
    @DisplayName("login fail : USERNAME_NOT_FOUND")
    @WithMockUser
    void login_fail_USERNAME_NOT_FOUND() throws Exception {
        UserLoginRequest userLoginRequest = UserLoginRequest.builder()
                .userName("헝지")
                .password("1234")
                .build();
        when(userService.login(any(),any())).thenThrow(new FinalProjectAppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));

        mockMvc.perform(post("/api/v1/users/login")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(userLoginRequest)))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.resultCode").value("ERROR"))
                .andExpect(jsonPath("$.result.errorCode").value(ErrorCode.USERNAME_NOT_FOUND.toString()))
                .andExpect(jsonPath("$.result.message").value(ErrorCode.USERNAME_NOT_FOUND.getMessage()));

    }

    @Test
    @DisplayName("login fail : INVALID_PASSWORD")
    @WithMockUser
    void login_fail_INVALID_PASSWORD() throws Exception {
        UserLoginRequest userLoginRequest = UserLoginRequest.builder()
                .userName("헝지")
                .password("1234")
                .build();
        when(userService.login(any(),any())).thenThrow(new FinalProjectAppException(ErrorCode.INVALID_PASSWORD, ErrorCode.INVALID_PASSWORD.getMessage()));

        mockMvc.perform(post("/api/v1/users/login")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(userLoginRequest)))
                .andDo(print())
                .andExpect(status().isUnauthorized())
                .andExpect(jsonPath("$.resultCode").value("ERROR"))
                .andExpect(jsonPath("$.result.errorCode").value(ErrorCode.INVALID_PASSWORD.toString()))
                .andExpect(jsonPath("$.result.message").value(ErrorCode.INVALID_PASSWORD.getMessage()));

    }
}