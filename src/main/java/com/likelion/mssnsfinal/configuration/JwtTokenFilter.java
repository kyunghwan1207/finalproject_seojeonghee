package com.likelion.mssnsfinal.configuration;

import com.likelion.mssnsfinal.domain.ErrorResponse;
import com.likelion.mssnsfinal.domain.Response;
import com.likelion.mssnsfinal.domain.entity.User;
import com.likelion.mssnsfinal.exception.ErrorCode;
import com.likelion.mssnsfinal.exception.FinalProjectAppException;
import com.likelion.mssnsfinal.repository.UserRepository;
import com.likelion.mssnsfinal.service.UserService;
import com.likelion.mssnsfinal.utils.JwtTokenUtil;
import io.jsonwebtoken.ExpiredJwtException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
public class JwtTokenFilter extends OncePerRequestFilter {
    private final String secretKey;
    private final UserService userService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        final String authorizationHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        log.info("authorizationHeader : {}", authorizationHeader);

        if(authorizationHeader==null || !authorizationHeader.startsWith("Bearer")){
            request.setAttribute("exception", ErrorCode.INVALID_PERMISSION);
            filterChain.doFilter(request,response);
            return;
        }

        String token;
        try {
            token = authorizationHeader.split(" ")[1];
        }
        catch (Exception e){
            request.setAttribute("exception", ErrorCode.INVALID_PERMISSION);
            filterChain.doFilter(request,response);
            return;
        }
        Integer userId;
        try {
            userId = JwtTokenUtil.getUserId(token,secretKey);
            log.info("user Name :{}", userId);
        }
        catch (Exception e){
            request.setAttribute("exception", ErrorCode.INVALID_TOKEN);
            filterChain.doFilter(request,response);
            return;
        }

        User user = userService.getUserByUserId(userId);
        log.info("userRole: {}",user.getRole());

        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(user.getUserId(), null, List.of(new SimpleGrantedAuthority(user.getRole().name())));
        authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        request.setAttribute("exception", null);
        filterChain.doFilter(request, response);
        log.info("user Name :{}", userId);

    }
}
