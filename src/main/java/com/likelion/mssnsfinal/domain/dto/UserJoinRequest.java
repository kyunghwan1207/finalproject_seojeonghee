package com.likelion.mssnsfinal.domain.dto;

import com.likelion.mssnsfinal.domain.UserRole;
import com.likelion.mssnsfinal.domain.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class UserJoinRequest {
    private String userName;
    private String password;


    public User toEntity(String password){
        return User.builder()
                .userName(this.userName)
                .password(password)
                .registeredAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .role(UserRole.USER)
                .build();
    }
}
