package com.likelion.mssnsfinal.service;

import com.likelion.mssnsfinal.domain.UserRole;
import com.likelion.mssnsfinal.domain.dto.*;
import com.likelion.mssnsfinal.domain.entity.Post;
import com.likelion.mssnsfinal.domain.entity.User;
import com.likelion.mssnsfinal.exception.ErrorCode;
import com.likelion.mssnsfinal.exception.FinalProjectAppException;
import com.likelion.mssnsfinal.repository.PostRepository;
import com.likelion.mssnsfinal.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PostService {
    private final PostRepository postRepository;
    private final UserRepository userRepository;

    public PostInfoResponse getPost(Integer id){
        Post selectedPost = postRepository.findById(id)
                .orElseThrow(()-> new FinalProjectAppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));
        return PostInfoResponse.builder()
                .id(selectedPost.getId())
                .title(selectedPost.getTitle())
                .body(selectedPost.getBody())
                .userName(selectedPost.getUser().getUserName())
                .createdAt(selectedPost.getCreatedAt())
                .lastModifiedAt(selectedPost.getLastModifiedAt())
                .build();
    }

    public List<PostInfoResponse> getAllPosts(Pageable pageable){
        Page<Post> postPages = postRepository.findAll(pageable);
        List<Post> postLists = new ArrayList<>();
        List<PostInfoResponse> postInfoResponses = new ArrayList<>();
        if(postPages!=null && postPages.hasContent()){
            postLists = postPages.getContent();
        }
        for (Post postList : postLists) {
            postInfoResponses.add(PostInfoResponse.builder()
                    .id(postList.getId())
                    .title(postList.getTitle())
                    .body(postList.getBody())
                    .userName(postList.getUser().getUserName())
                    .createdAt(postList.getCreatedAt())
                    .lastModifiedAt(postList.getLastModifiedAt())
                    .build());
        }
        return postInfoResponses;
    }

    public PostEnrollAndModifyAndDeleteResponse enrollPost(PostEnrollAndModifyRequest request, String userId){
        User user = userRepository.findById(Integer.parseInt(userId))
                .orElseThrow(()-> new FinalProjectAppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));
        Post savedPost = postRepository.save(request.toEntityForEnroll(user));
        return PostEnrollAndModifyAndDeleteResponse.builder()
                .message("포스트 등록 완료")
                .postId(savedPost.getId())
                .build();
    }

    public PostEnrollAndModifyAndDeleteResponse modifyPost(Integer id, PostEnrollAndModifyRequest request, String userId){
        User user = userRepository.findById(Integer.parseInt(userId))
                .orElseThrow(()-> new FinalProjectAppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));
        Post before = postRepository.findById(id).orElseThrow(()-> new FinalProjectAppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));
        if(!(before.getUser().getUserId()==user.getUserId()||user.getRole().equals(UserRole.ADMIN))){
            throw new FinalProjectAppException(ErrorCode.INVALID_PERMISSION,ErrorCode.INVALID_PASSWORD.getMessage());
        }
        Post after = postRepository.save(request.toEntityForModify(user,before));
        return PostEnrollAndModifyAndDeleteResponse.builder()
                .message("포스트 수정 완료")
                .postId(after.getId())
                .build();
    }

    public PostEnrollAndModifyAndDeleteResponse deletePost(Integer id, String userId){
        User user = userRepository.findById(Integer.parseInt(userId))
                .orElseThrow(()-> new FinalProjectAppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));
        Post post = postRepository.findById(id).orElseThrow(()-> new FinalProjectAppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));
        if(!(post.getUser().getUserId()==user.getUserId()||user.getRole().equals(UserRole.ADMIN))){
            throw new FinalProjectAppException(ErrorCode.INVALID_PERMISSION,ErrorCode.INVALID_PASSWORD.getMessage());
        }
        postRepository.deleteById(id);
        return PostEnrollAndModifyAndDeleteResponse.builder()
                .message("포스트 삭제 완료")
                .postId(post.getId())
                .build();
    }


}
