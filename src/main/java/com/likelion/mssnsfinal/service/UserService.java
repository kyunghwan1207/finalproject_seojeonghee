package com.likelion.mssnsfinal.service;

import com.likelion.mssnsfinal.domain.dto.UserDto;
import com.likelion.mssnsfinal.domain.dto.UserJoinRequest;
import com.likelion.mssnsfinal.domain.entity.User;
import com.likelion.mssnsfinal.exception.ErrorCode;
import com.likelion.mssnsfinal.exception.FinalProjectAppException;
import com.likelion.mssnsfinal.repository.UserRepository;
import com.likelion.mssnsfinal.utils.JwtTokenUtil;
import io.swagger.models.auth.In;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder;

    @Value("${jwt.token.secret}")
    private
    String secretKey;
    private long expireTimeMs = 1000 * 60 * 60 * 10;

    public UserDto join(UserJoinRequest request){
        userRepository.findByUserName(request.getUserName())
                .ifPresent(user -> {
                    throw new FinalProjectAppException(ErrorCode.DUPLICATED_USER_NAME, ErrorCode.DUPLICATED_USER_NAME.getMessage());
                });

        User savedUser = userRepository.save(request.toEntity(encoder.encode(request.getPassword())));
        return UserDto.builder()
                .userId(savedUser.getUserId())
                .userName(savedUser.getUserName())
                .registeredAt(savedUser.getRegisteredAt())
                .updatedAt(savedUser.getUpdatedAt())
                .role(savedUser.getRole())
                .build();
    }

    public String login(String userName, String password) {
        User user = userRepository.findByUserName(userName)
                .orElseThrow(()-> new FinalProjectAppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));
        if(!encoder.matches(password,user.getPassword())){
            throw new FinalProjectAppException(ErrorCode.INVALID_PASSWORD, ErrorCode.INVALID_PASSWORD.getMessage());
        }

        return JwtTokenUtil.createToken(user.getUserId(), secretKey, expireTimeMs);
    }

    public User getUserByUserId(Integer userId) {
        return userRepository.findById(userId)
                .orElseThrow(()->new FinalProjectAppException(ErrorCode.USERNAME_NOT_FOUND, ""));
    }
}
