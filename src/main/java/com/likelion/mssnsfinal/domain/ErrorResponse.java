package com.likelion.mssnsfinal.domain;

import com.likelion.mssnsfinal.exception.ErrorCode;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class ErrorResponse {
    private ErrorCode errorCode;
    private String message;
}
