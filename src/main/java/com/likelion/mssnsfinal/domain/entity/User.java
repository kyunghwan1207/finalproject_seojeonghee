package com.likelion.mssnsfinal.domain.entity;

import com.likelion.mssnsfinal.domain.UserRole;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer userId;
    @NotNull
    @Column(unique = true, nullable = false)
    private String userName;
    @NotNull
    @Column(nullable = false)
    private String password;
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private UserRole role;
    @NotNull
    @Column(nullable = false)
    private LocalDateTime registeredAt;
    @NotNull
    @Column(nullable = false)
    private LocalDateTime updatedAt;


}