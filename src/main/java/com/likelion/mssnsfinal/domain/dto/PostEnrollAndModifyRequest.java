package com.likelion.mssnsfinal.domain.dto;

import com.likelion.mssnsfinal.domain.entity.Post;
import com.likelion.mssnsfinal.domain.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class PostEnrollAndModifyRequest {
    private String title;
    private String body;

    public Post toEntityForEnroll(User user){
        return Post.builder()
                .title(this.title)
                .body(this.body)
                .createdAt(LocalDateTime.now())
                .lastModifiedAt(LocalDateTime.now())
                .user(user)
                .build();
    }
    public Post toEntityForModify(User user,Post post){
        return Post.builder()
                .id(post.getId())
                .title(this.title)
                .body(this.body)
                .createdAt(post.getCreatedAt())
                .lastModifiedAt(LocalDateTime.now())
                .user(user)
                .build();
    }

}
