package com.likelion.mssnsfinal.controller;

import com.likelion.mssnsfinal.domain.Response;
import com.likelion.mssnsfinal.domain.dto.PostEnrollAndModifyRequest;
import com.likelion.mssnsfinal.domain.dto.PostEnrollAndModifyAndDeleteResponse;
import com.likelion.mssnsfinal.domain.dto.PostInfoResponse;
import com.likelion.mssnsfinal.domain.entity.User;
import com.likelion.mssnsfinal.service.PostService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.*;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/v1/posts")
@RequiredArgsConstructor
public class PostController {
    private final PostService postService;

    @GetMapping("")
    public Response<Page<PostInfoResponse>> getAllPosts(@PageableDefault(size = 20, sort ="createdAt",
            direction = Sort.Direction.DESC) Pageable pageable) {
        List<PostInfoResponse> posts = postService.getAllPosts(pageable);
        return Response.success(new PageImpl<>(posts));
    }

    @GetMapping ("/{id}")
    public Response<PostInfoResponse> getPost(@PathVariable Integer id){
        PostInfoResponse postInfoResponse = postService.getPost(id);
        return Response.success(postInfoResponse);
    }

    @PostMapping
    public Response<PostEnrollAndModifyAndDeleteResponse> writePost(@RequestBody PostEnrollAndModifyRequest postEnrollAndModifyRequest, Authentication authentication){
        PostEnrollAndModifyAndDeleteResponse postEnrollAndModifyAndDeleteResponse = postService.enrollPost(postEnrollAndModifyRequest,authentication.getName());
        log.info("Controller userName : {}", authentication);
        return Response.success(postEnrollAndModifyAndDeleteResponse);
    }

    @PutMapping("/{id}")
    public Response<PostEnrollAndModifyAndDeleteResponse> modifyPost(@PathVariable Integer id, @RequestBody PostEnrollAndModifyRequest postEnrollAndModifyRequest, Authentication authentication){
        PostEnrollAndModifyAndDeleteResponse postEnrollAndModifyAndDeleteResponse = postService.modifyPost(id, postEnrollAndModifyRequest, authentication.getName());
        log.info("Controller userName : {}", authentication);
        return Response.success(postEnrollAndModifyAndDeleteResponse);

    }

    @DeleteMapping("/{id}")
    public Response<PostEnrollAndModifyAndDeleteResponse> deletePost(@PathVariable Integer id, Authentication authentication){
        PostEnrollAndModifyAndDeleteResponse postEnrollAndModifyAndDeleteResponse = postService.deletePost(id,authentication.getName());
        log.info("Controller userName : {}", authentication);
        return Response.success(postEnrollAndModifyAndDeleteResponse);

    }

}
