package com.likelion.mssnsfinal.domain;

public enum UserRole {
    ADMIN, USER
}
