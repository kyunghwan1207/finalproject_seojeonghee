package com.likelion.mssnsfinal.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.likelion.mssnsfinal.domain.dto.PostEnrollAndModifyAndDeleteResponse;
import com.likelion.mssnsfinal.domain.dto.PostEnrollAndModifyRequest;
import com.likelion.mssnsfinal.domain.dto.PostInfoResponse;
import com.likelion.mssnsfinal.domain.entity.Post;
import com.likelion.mssnsfinal.domain.entity.User;
import com.likelion.mssnsfinal.exception.ErrorCode;
import com.likelion.mssnsfinal.exception.FinalProjectAppException;
import com.likelion.mssnsfinal.service.PostService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.awt.print.Pageable;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.data.domain.Sort.Direction.DESC;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(PostController.class)
class PostControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private PostService postService;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    @DisplayName("get post list success")
    @WithMockUser
    void get_post_list_success() throws Exception {
        mockMvc.perform(get("/api/v1/posts")
                        .param("sort", "createdAt,desc"))
                .andExpect(status().isOk())
                .andDo(print());
        ArgumentCaptor<Pageable> pageableCaptor = ArgumentCaptor.forClass(Pageable.class);
        verify(postService).getAllPosts((org.springframework.data.domain.Pageable) pageableCaptor.capture());
        PageRequest pageable = (PageRequest) pageableCaptor.getValue();

        assertEquals(Sort.by(DESC, "createdAt"), pageable.getSort());

    }

    @Test
    @DisplayName("get post success")
    @WithMockUser
    void get_post_success() throws Exception {
        PostInfoResponse postInfoResponse = PostInfoResponse.builder()
                .id(1)
                .title("하이")
                .body("안녕")
                .userName("헝지")
                .build();
        when(postService.getPost(any())).thenReturn(postInfoResponse);
        mockMvc.perform(get("/api/v1/posts/{id}",1)
                        .accept(MediaType.APPLICATION_JSON)
                        .with(csrf()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.resultCode").value("SUCCESS"))
                .andExpect(jsonPath("$.result.id").exists())
                .andExpect(jsonPath("$.result.body").exists())
                .andExpect(jsonPath("$.result.userName").exists())
                .andExpect(jsonPath("$.result.title").exists());
    }

    @Test
    @DisplayName("write post success")
    @WithMockUser
    void write_post_success() throws Exception {
        PostEnrollAndModifyRequest postEnrollAndModifyRequest = PostEnrollAndModifyRequest.builder()
                .body("안녕")
                .title("헬로")
                .build();
        PostEnrollAndModifyAndDeleteResponse postEnrollResponse = PostEnrollAndModifyAndDeleteResponse.builder()
                .message("포스트 등록 완료")
                .build();
        when(postService.enrollPost(any(),any())).thenReturn(postEnrollResponse);
        mockMvc.perform(post("/api/v1/posts")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(postEnrollAndModifyRequest)))
                .andDo(print())
                .andExpect(status().isOk());
    }
    @Test
    @DisplayName("write post failed_do not login")
    @WithAnonymousUser
    void post_fail_do_not_login() throws Exception{
        PostEnrollAndModifyRequest postEnrollRequest = PostEnrollAndModifyRequest.builder()
                .title("title")
                .body("body")
                .build();
        when(postService.enrollPost(any(),any())).thenThrow(new FinalProjectAppException(ErrorCode.INVALID_PERMISSION,ErrorCode.INVALID_PERMISSION.getMessage()));
        mockMvc.perform(post("/api/v1/posts")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(postEnrollRequest)))
                .andDo(print())
                .andExpect(status().isUnauthorized());

    }
    @Test
    @DisplayName("write post failed_token expired")
    @WithAnonymousUser
    void post_fail_token_expired() throws Exception{
        PostEnrollAndModifyRequest postEnrollRequest = PostEnrollAndModifyRequest.builder()
                .title("title")
                .body("body")
                .build();
        when(postService.enrollPost(any(),any())).thenThrow(new FinalProjectAppException(ErrorCode.INVALID_TOKEN,ErrorCode.INVALID_TOKEN.getMessage()));
        mockMvc.perform(post("/api/v1/posts")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(postEnrollRequest)))
                .andDo(print())
                .andExpect(status().isUnauthorized());

    }

    @Test
    @DisplayName("modify post success")
    @WithMockUser
    void modify_post_success() throws Exception {
        PostEnrollAndModifyRequest postEnrollAndModifyRequest = PostEnrollAndModifyRequest.builder()
                .body("수정")
                .title("수정")
                .build();
        Post post = mock(Post.class);
        PostEnrollAndModifyAndDeleteResponse postEnrollAndModifyAndDeleteResponse = PostEnrollAndModifyAndDeleteResponse.builder()
                .postId(post.getId())
                .message("포스트 수정 완료")
                .build();
        when(postService.modifyPost(any(),any(),any())).thenReturn(postEnrollAndModifyAndDeleteResponse);
        mockMvc.perform(put("/api/v1/posts/{id}",1)
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(postEnrollAndModifyRequest)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("modify post fail_do not login")
    @WithAnonymousUser
    void modify_post_fail_do_not_login() throws Exception {
        PostEnrollAndModifyRequest postEnrollAndModifyRequest = PostEnrollAndModifyRequest.builder()
                .body("수정")
                .title("수정")
                .build();
        when(postService.modifyPost(any(),any(),any())).thenThrow(new FinalProjectAppException(ErrorCode.INVALID_PERMISSION,ErrorCode.INVALID_PERMISSION.getMessage()));
        mockMvc.perform(put("/api/v1/posts/{id}",1)
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(postEnrollAndModifyRequest)))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    @DisplayName("modify post fail_user not matched")
    @WithMockUser
    void modify_post_fail_user_not_matched() throws Exception {
        PostEnrollAndModifyRequest postEnrollAndModifyRequest = PostEnrollAndModifyRequest.builder()
                .body("수정")
                .title("수정")
                .build();
        when(postService.modifyPost(any(),any(),any())).thenThrow(new FinalProjectAppException(ErrorCode.INVALID_PERMISSION,ErrorCode.INVALID_PERMISSION.getMessage()));
        mockMvc.perform(put("/api/v1/posts/{id}",1)
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(postEnrollAndModifyRequest)))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    @DisplayName("delete post success")
    @WithMockUser
    void delete_post_success() throws Exception {
        User mockUser = mock(User.class);
        PostEnrollAndModifyAndDeleteResponse postEnrollAndModifyAndDeleteResponse = PostEnrollAndModifyAndDeleteResponse.builder()
                .postId(1)
                .message("포스트 삭제 완료")
                .build();
        when(postService.deletePost(any(),any())).thenReturn(mock(PostEnrollAndModifyAndDeleteResponse.class));
        mockMvc.perform(delete("/api/v1/posts/{id}",1)
                        .accept(MediaType.APPLICATION_JSON)
                        .with(csrf()))
                .andExpect(status().isOk());

    }

    @Test
    @DisplayName("delete post fail_do not login")
    @WithAnonymousUser
    void delete_post_fail_do_not_login() throws Exception {
        PostEnrollAndModifyAndDeleteResponse postEnrollAndModifyAndDeleteResponse = PostEnrollAndModifyAndDeleteResponse.builder()
                .postId(1)
                .message("포스트 삭제 완료")
                .build();
        when(postService.modifyPost(any(),any(),any())).thenThrow(new FinalProjectAppException(ErrorCode.INVALID_PERMISSION,ErrorCode.INVALID_PERMISSION.getMessage()));
        mockMvc.perform(put("/api/v1/posts/{id}",1)
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(postEnrollAndModifyAndDeleteResponse)))
                .andDo(print())
                .andExpect(status().isUnauthorized());

    }
    @Test
    @DisplayName("delete post fail_user not matched")
    @WithMockUser
    void delete_post_fail_user_not_matched() throws Exception {
        PostEnrollAndModifyAndDeleteResponse postEnrollAndModifyAndDeleteResponse = PostEnrollAndModifyAndDeleteResponse.builder()
                .postId(1)
                .message("포스트 삭제 완료")
                .build();
        when(postService.deletePost(any(),any())).thenThrow(new FinalProjectAppException(ErrorCode.INVALID_PERMISSION,ErrorCode.INVALID_PERMISSION.getMessage()));
        mockMvc.perform(delete("/api/v1/posts/{id}",1)
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(postEnrollAndModifyAndDeleteResponse)))
                .andDo(print())
                .andExpect(status().isUnauthorized());

    }
}